﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [SerializeField] private GameObject m_T_Tetramino;
    [SerializeField] private GameObject m_S_Tetramino;
    [SerializeField] private GameObject m_Z_Tetramino;
    [SerializeField] private GameObject m_O_Tetramino;
    [SerializeField] private GameObject m_L_Tetramino;
    [SerializeField] private GameObject m_J_Tetramino;
    [SerializeField] private GameObject m_I_Tetramino;
    [SerializeField] private GameObject m_startPosition;
    [SerializeField] private GameObject m_nextTetramino;

    GameObject[] tetraminos;

    private GameObject startTetramino;
    private GameObject nextTetramino;

    int randomNumber;

    // Start is called before the first frame update
    void Start()
    {
        tetraminos = new GameObject[7];
        tetraminos[0] = m_T_Tetramino;
        tetraminos[1] = m_S_Tetramino;
        tetraminos[2] = m_Z_Tetramino;
        tetraminos[3] = m_O_Tetramino;
        tetraminos[4] = m_L_Tetramino;
        tetraminos[5] = m_J_Tetramino;
        tetraminos[6] = m_I_Tetramino;

        randomNumber = Random.Range(0, 7);

        startTetramino = Instantiate(tetraminos[randomNumber]);
        startTetramino.transform.position = m_startPosition.transform.position;

        randomNumber = Random.Range(0, 7);

        nextTetramino = Instantiate(tetraminos[randomNumber]);
        nextTetramino.transform.position = m_nextTetramino.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 position = startTetramino.transform.position;
        position.y -= 0.2f;
        startTetramino.transform.position = position;

        if(position.y < 0)
        {
            Destroy(startTetramino);
            startTetramino = nextTetramino;
            startTetramino.transform.position = m_startPosition.transform.position;

            randomNumber = Random.Range(0, 7);
            nextTetramino = Instantiate(tetraminos[randomNumber]);
            nextTetramino.transform.position = m_nextTetramino.transform.position;
        }

    }
    
}
