﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameFieldGenerator : MonoBehaviour
{
    public const int GAME_FIELD_HEIGHT = 22;
    public const int GAME_FIELD_WIDTH = 10;

    // Start is called before the first frame update
    void Start()
    {
        GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        GameObject wallCube;

        //Создание дна стакана
        for (int i = 0; i < GAME_FIELD_WIDTH - 1; i++)
        {
            wallCube = Instantiate(cube);
            wallCube.transform.position = new Vector3(x: (float)i + 1.0f, y: 0.0f, z: 0.0f);
        }

        //Создание стен стакана
        for (int j = 0; j < GAME_FIELD_HEIGHT; j++)
        {
            //Левая стена
            wallCube = Instantiate(cube);
            wallCube.transform.position = new Vector3(x: -1.0f, y: (float)j, z: 0.0f);

            //Правая стена
            wallCube = Instantiate(cube);
            wallCube.transform.position = new Vector3(x: (float)GAME_FIELD_WIDTH, y: (float)j, z: 0.0f);
        }
        
    }

}
